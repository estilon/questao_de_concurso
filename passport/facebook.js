var FacebookStrategy = require('passport-facebook').Strategy;
var Pessoa = require('../persistence/models/pessoa');
var fbConfig = require('../config/facebook_config');

module.exports = function(passport){

    passport.use('facebook', new FacebookStrategy({
        clientID        : fbConfig.appID,
        clientSecret    : fbConfig.appSecret,
        callbackURL     : fbConfig.callbackUrl
        },

        function(access_token, refresh_token, profile, done){

            process.nextTick(function(){
                Pessoa.findOne({'id' : profile.id},function(err, pessoa){

                    if(err)
                        return done(err);

                    if(pessoa){
                        return done(null, pessoa);
                    }else{

                        var novaPessoa = new Pessoa();

                        novaPessoa.id = profile.id;
                        novaPessoa.access_token = access_token;
                        novaPessoa.firstName  = profile.name.givenName;
                        novaPessoa.lastName = profile.name.familyName;
                        novaPessoa.email = ""/*profile.emails[0].value;*/;

                        novaPessoa.save(function(err){
                            if(err)
                                throw err;
                            return done(null,novaPessoa);
                        });
                    }
                });
            });

        }));



};



