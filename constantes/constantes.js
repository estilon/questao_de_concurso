var constantes = {
    MSG_NAO_IMPLEMENTADO : "Ainda não implementado",
    MSG_SUCESSO : "sucesso",
    MSG_FALHA : "falha",
    FALHA_DE_BANCO : 1,
    TOKEN_INVALIDO : 2,
    TOKEN_VALIDO : 3,
    NOTIFICACAO_NOVA_QUESTAO : "nova_questao"
};

module.exports = constantes;