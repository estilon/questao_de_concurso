var express = require('express');
var router = express.Router();
var path    = require("path")

/* GET home page. */
router.get('/', function(req, res, next) {
  res.sendFile(path.join(__dirname+"/../public/app/index.html"));
});

router.get('/login-page',function(req,res, next){
  res.sendFile(path.join(__dirname+"/../public/app/login.html"));
});

module.exports = router;
