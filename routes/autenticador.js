var express = require('express');
var router = express.Router();
var Pessoa = require('../persistence/models/pessoa');
var constantes = require('../constantes/constantes');



router.post('/login', function(req, res, next) {
    var email = req.body.email;
    var senha = req.body.senha;

    if(req.session.autenticado == true)
        res.json({status: constantes.MSG_SUCESSO});

    else

        Pessoa.findOne({email : email, senha : senha},function(err, pessoa){
            if(err || pessoa == null)
                res.json({status : constantes.MSG_FALHA});
            else {
                req.session.autenticado = true;
                res.json({status: constantes.MSG_SUCESSO, id : pessoa._id});
            }
        });
});

router.get('/logout',function(req, res, next){
    req.session.autenticado = false;

    res.json({staus : constantes.MSG_SUCESSO});
});

router.get("/",function(req,res,next){
    if(req.session.autenticado == true)
        next();
    else
        res.redirect("/login-page");
});

router.get("/users*",function(req,res,next){
    if(req.session.autenticado == true)
        next();
    else
        res.redirect("/login-page");
});

router.post("/users*",function(req,res,next){
    if(req.session.autenticado == true)
        next();
    else
        res.redirect("/login-page");
});

module.exports = router;
